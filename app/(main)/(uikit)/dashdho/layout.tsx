'use client';
// import { Metadata } from 'next'; // imported from another file
import { LayoutProvider } from './layout_one/context/layoutcontext';
import { PrimeReactProvider } from 'primereact/api';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import 'primeicons/primeicons.css';
import '../../../../styles/layout/layout.scss';
import '../../../../styles/demo/Demos.scss';
import Layout from './layout_one/layout'; // imported from another file

interface RootLayoutProps {
    children: React.ReactNode;
}

// imported from another file

// export const metadata: Metadata = {
//     title: 'PrimeReact Sakai',
//     description: 'The ultimate collection of design-agnostic, flexible and accessible React UI Components.',
//     robots: { index: false, follow: false },
//     viewport: { initialScale: 1, width: 'device-width' },
//     openGraph: {
//         type: 'website',
//         title: 'PrimeReact SAKAI-REACT',
//         url: 'https://sakai.primereact.org/',
//         description: 'The ultimate collection of design-agnostic, flexible and accessible React UI Components.',
//         images: ['https://www.primefaces.org/static/social/sakai-react.png'],
//         ttl: 604800
//     },
//     icons: {
//         icon: '/favicon.ico'
//     }
// };

// imported from another file

export default function RootLayout({ children }: RootLayoutProps) {
    return (
        <html lang="en" suppressHydrationWarning>
            <head>
                <link id="theme-css" href={`/themes/lara-light-indigo/theme.css`} rel="stylesheet"></link>
            </head>
            <body>
                <PrimeReactProvider>
                    <LayoutProvider>
                        <Layout>{children}</Layout>
                    </LayoutProvider>
                </PrimeReactProvider>
            </body>
        </html>
    );
}