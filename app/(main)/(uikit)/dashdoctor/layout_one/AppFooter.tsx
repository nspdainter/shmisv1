/* eslint-disable @next/next/no-img-element */

import React, { useContext } from 'react';
import { LayoutContext } from './context/layoutcontext';

const AppFooter = () => {
    const { layoutConfig } = useContext(LayoutContext);

    return (
        <div className="layout-footer">
            <img src={`/layout/images/uhai-logo.svg`} width="55" height={'35px'} alt="logo" />
            by
            <span className="font-medium ml-2">ILERA INITIATIVE LTD</span>
        </div>
    );
}; 

export default AppFooter;
