// Tabs.tsx
import { ReactNode, useState } from 'react';

interface TabsProps {
  tabs: string[];
  defaultTab: number;
  children: ReactNode[];
}

const Tabs: React.FC<TabsProps> = ({ tabs, defaultTab, children }) => {
  const [activeTab, setActiveTab] = useState<number>(defaultTab);

  const handleTabClick = (tab: number) => {
    setActiveTab(tab);
  };

  return (
    <div style={{ overflowX: 'auto' }}>
      <div style={{ whiteSpace: 'nowrap', display: 'flex' }}>
        {tabs.map((tab, index) => (
          <button
            key={tab}
            onClick={() => handleTabClick(index)}
            style={{
              marginRight: '10px',
              padding: '5px 10px',
              cursor: 'pointer',
              background: activeTab === index ? 'lightblue' : 'transparent',
            }}
          >
            {tab}
          </button>
        ))}
      </div>
      <div>{children[activeTab]}</div>
    </div>
  );
};

export default Tabs;
